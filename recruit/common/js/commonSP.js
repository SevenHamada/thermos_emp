$(function() {
	// ページ内スクロール
    var headerHight = 40;
	var notList = '.dataList a';
    $('a[href^=#]').not(notList).click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top-headerHight;
        $("html, body").not(':animated').animate({scrollTop:position}, 550, "swing");
        return false;
    });
	//ページ上部へ戻る
	$("#pageTop").click(function () {
		$('html,body').animate({ scrollTop: 0 }, 'easeInOutQuint');
		return false;
	});


	//サイドメニューの表示
	var menu = $('#menu'), 
		menuBtn = $('.navBtn'), 
		body = $(document.body),     
		menuWidth = menu.outerWidth(); 
		menuBtn.click(function(){
		body.toggleClass('open');
			if(body.hasClass('open')){
				menu.animate({'right' : 0 }, 'easeInOutQuint');
				$('body').css({'overflow-y' : 'hidden' });                  
			} else {
				menu.animate({'right' : -menuWidth }, 'easeInOutQuint');
				$('body').css({'overflow-y' : 'inherit' });                  
			}  
			if ($(this).hasClass('is-open')) {
			  $(this).removeClass('is-open');
			} else {
			  $(this).addClass('is-open');
			}          
	});


	//メニュー開閉
	$(".recruit .loNav h3").click(function(){
		$(this).next("ul").slideToggle();
		$(this).toggleClass("open");    
		$(this).siblings("ul").removeClass("open");
		if ($(".recruit .loNav").hasClass('rec-open')) {
		   $(".recruit .loNav").removeClass('rec-open');
		} else {
		   $(".recruit .loNav").addClass('rec-open');
		}   
	});
	
	
	$(".qa dt").click(function(){
		$(this).next("dd").slideToggle();
		$(this).toggleClass("open");    
	});
	
	

	 
});