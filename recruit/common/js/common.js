// onoff
function smartRollover() {
	if(document.getElementsByTagName) {
		var images = document.getElementsByTagName("img");

		for(var i=0; i < images.length; i++) {
			if(images[i].getAttribute("src").match("_off."))
			{
				images[i].onmouseover = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_off.", "_on."));
				}
				images[i].onmouseout = function() {
					this.setAttribute("src", this.getAttribute("src").replace("_on.", "_off."));
				}
			}
		}
	}
}

if(window.addEventListener) {
	window.addEventListener("load", smartRollover, false);
}
else if(window.attachEvent) {
	window.attachEvent("onload", smartRollover);
}
 
 

// ページ遷移
$(function() {
    $("#contents").css({'top':'0'}).fadeTo(0,0).delay(200).animate({'opacity':1,'top':'0px'},700);
});

//メニュー開閉
$(function(){
    $("ul.sub_list")
		$(".navBtn").mouseenter(function(){
            $(".navBtn").stop(true, true).css({'bottom':'0px','height':'65px','overflow':'hidden'}).animate({'opacity':1,'bottom':'37px','height':'28px'},450);
            $(".navWrap").stop(true, true).css({'bottom':'0px'}).animate({'opacity':1,'bottom':'55px'},400);
            $("nav").stop(true, true).css({'bottom':'0px'}).fadeTo(0,100).delay(0).animate({'opacity':1,'bottom':'-10px'},400);
            $("#pageTop").stop(true, true).css({'z-index':'5'});
		});
		$(".navBtn01").mouseenter(function(){
			$(".navBtn01").stop(true, true).css({'bottom':'-10px','height':'65px','overflow':'hidden'}).animate({'opacity':1,'bottom':'0px','height':'30px'},450);
            $(".navWrap").stop(true, true).css({'bottom':'0px'}).animate({'opacity':1,'bottom':'55px'},400);
            $("nav").stop(true, true).css({'bottom':'0px'}).fadeTo(0,100).delay(0).animate({'opacity':1,'bottom':'-10px'},400);
            $("#pageTop").stop(true, true).css({'z-index':'5'});
		});
		$(".navWrap").mouseleave(function(){
            $(".navBtn").stop(true, true).css({'bottom':'37px','height':'28px','overflow':'inherit'}).animate({'opacity':1,'bottom':'10px','height':'65px'},500);
            $(".navBtn01").stop(true, true).css({'bottom':'0px','height':'65px','overflow':'inherit'}).animate({'opacity':1,'bottom':'-0px','height':'120px'},800);
            $(".navWrap").stop(true, true).css({'bottom':'55px'}).animate({'opacity':1,'bottom':'0px'},800);
            $("#pageTop").stop(true, true).css({'z-index':'101'});
		});
})

// ページ内スクロール
$(function() {

	$('a[href^=#]').click(function(){
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top - 50;
		$("html, body").not(':animated').animate({scrollTop:position}, speed, "easeInOutQuint");
		return false;
	});
		//ページ上部へ戻る
	$("#pageTop").click(function () {
		$('html,body').animate({ scrollTop: 0 }, 'easeInOutQuint');
		return false;
	});
});

// ナビ振り分け
jQuery(document).ready(function(){
if (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
        $('.spUser').show();
    } else {
        $('.pcUser').show();
}
});