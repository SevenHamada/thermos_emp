// history scroll
$(function() {

	/**
	 * 現在スクロール位置によるグローバルナビのアクティブ表示
	 */
	var scrollMenu = function() {
		// 配列宣言
		// ここにスクロールで点灯させる箇所のidを記述する
		// 数値は全て0でOK
		var array = {
			'#history01': 0,
			'#history02': 0,
			'#history03': 0,
			'#history04': 0,
			'#history05': 0,
			'#history06': 0,
			'#history07': 0,
			'#history08': 0,
			'#history09': -10,
			'#history10': -20,
			'#history11': -40,
			'#history12': -80,
			'#history13': -160,
		};

		var $globalNavi = new Array();

		// 各要素のスクロール値を保存
		for (var key in array) {
			if ($(key).offset()) {
				array[key] = $(key).offset().top - 50; // 数値丁度だとずれるので10px余裕を作る
				$globalNavi[key] = $('#hList ul li a[href="' + key + '"]');
			}
		}

		// スクロールイベントで判定
		$(window).scroll(function () {
			for (var key in array) {
				if ($(window).scrollTop() > array[key] - 50) {
					$('#hList ul li a').each(function() {
						$(this).removeClass('active');
					});
					$globalNavi[key].addClass('active');
				}
			}
		});
	}

	// 実行
	scrollMenu();
	
// historyLIst fixed
var nav    = $('#hList nav'),
    offset = nav.offset();

$(window).scroll(function () {
  if($(window).scrollTop() > offset.top - 20) {
    nav.addClass('fixed');
  } else {
    nav.removeClass('fixed');
  }
});

});

